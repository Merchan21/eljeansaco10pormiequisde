package facci.juliointriago.mongobd.rest.model;

import com.google.gson.annotations.SerializedName;

public class Post {
    @SerializedName("cedula")
    private String cedula;

    @SerializedName("ciudad")
    private String ciudad;

    @SerializedName("biografia")
    private String biografia;

    @SerializedName("nombre")
    private String nombre;

    @SerializedName("apellido")
    private String apellido;


    public Post(String cedula, String ciudad, String biografia, String nombre, String apellido) {
        this.cedula = cedula;
        this.ciudad = ciudad;
        this.biografia = biografia;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public String getCedula() {
        return cedula;
    }

    public String getCiudad() {
        return ciudad;
    }

    public String getBiografia() {
        return biografia;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }
}



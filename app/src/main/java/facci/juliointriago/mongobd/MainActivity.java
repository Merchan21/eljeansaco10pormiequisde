package facci.juliointriago.mongobd;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import facci.juliointriago.mongobd.rest.adapter.MarketAdapter;
import facci.juliointriago.mongobd.rest.model.Post;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    TextView cedula, ciudad, biografia, nombre, apellido;
    Button buttonPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        cedula = findViewById(R.id.cedula);
        ciudad = findViewById(R.id.ciudad);
        biografia = findViewById(R.id.biografia);
        nombre = findViewById(R.id.nombre);
        apellido = findViewById(R.id.apellido);
        buttonPost = findViewById(R.id.ButtonPost);

        buttonPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PostPosts();
                Intent i = new Intent(MainActivity.this, activity_recycler.class);
                startActivity(i);
            }
        });
    }


    private void PostPosts() {
        MarketAdapter adapter = new MarketAdapter();
        Call<Post> call = adapter.InsertPost(new Post(cedula.getText().toString(), ciudad.getText().toString(),biografia.getText().toString(),nombre.getText().toString(),apellido.getText().toString()));
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                Log.e("Hola", response.body().toString());
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {

            }
        });
    }
}
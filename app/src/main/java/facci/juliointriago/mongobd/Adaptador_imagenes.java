package facci.juliointriago.mongobd;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;

import facci.juliointriago.mongobd.rest.model.Post;

public class Adaptador_imagenes extends RecyclerView.Adapter<Adaptador_imagenes.ViewHolderImagenes> {
    ArrayList<Post> listaImagenes;

    public Adaptador_imagenes(ArrayList<Post> listaImagenes) {
        this.listaImagenes = listaImagenes;
    }

    @NonNull
    @Override
    public Adaptador_imagenes.ViewHolderImagenes onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, null, false);
        return new ViewHolderImagenes(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Adaptador_imagenes.ViewHolderImagenes holder, int position) {

        holder.cedula.setText(listaImagenes.get(position).getCedula());
        holder.ciudad.setText(listaImagenes.get(position).getCiudad());
        holder.biografia.setText(listaImagenes.get(position).getBiografia());
        holder.nombre.setText(listaImagenes.get(position).getNombre());
        holder.apellido.setText(listaImagenes.get(position).getApellido());




    }

    @Override
    public int getItemCount() {
        return listaImagenes.size();
    }

    public class ViewHolderImagenes extends RecyclerView.ViewHolder {

        TextView cedula, ciudad, biografia, nombre, apellido;

        public ViewHolderImagenes(View itemView) {
            super(itemView);

            cedula = (TextView) itemView.findViewById(R.id.cedula);
            ciudad = (TextView)itemView.findViewById(R.id.nombre);
            biografia = (TextView) itemView.findViewById(R.id.biografia);
            nombre = (TextView) itemView.findViewById(R.id.nombre);
            apellido = (TextView) itemView.findViewById(R.id.apellido);
        }
    }
}


